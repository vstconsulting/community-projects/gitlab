# Quick GitLab deployment

Project for quick GitLab deployment with [Polemarch](https://github.com/vstconsulting/polemarch).

## Supported OS

This project is able to deploy GitLab  on hosts with following OS:

* Ubuntu;
* Debian;
* RedHat;
* CentOS.

## Project's playbook

* `deploy_gitlab.yml` - this playbook deploys latest GitLab-CE service version,
* `make_gitlab_backup.yml` - creates a backup of your GitLab and copies to the storage of your choice.

### Available variables
| **Inventory**                              | **Environment**                    | **Description**             |
| ------------------------------------------ | ---------------------------------- | --------------------------- |
| `gitlab_name_host`  | - | Specifies the name of your domain. |

### To backup, the following variables are used
| **Inventory**                              | **Environment**                    | **Description**             |
| ------------------------------------------ | ---------------------------------- | --------------------------- |
| `gitlab_backup_save_count` | `env_GITLAB_BACKUP_SAVE_COUNT` | Count of backups stored. (default - `7`)  |
| `gitlab_backup_file` | `env_GITLAB_BACKUP_FILE` | Name backup file for restore |
| `gitlab_backup_storage_type` | `env_GITLAB_BACKUP_STORAGE_TYPE` | Storage type (local storage is used by default - `default`; available - `S3`) |
| `gitlab_backup_storage_dir` | `env_GITLAB_BACKUP_STORAGE_DIR` | Directory name for storing backups in system path (for default) or in bucket (for S3) |

#### Storage type - `S3`
| **Inventory**                              | **Environment**                    | **Description**             |
| ------------------------------------------ | ---------------------------------- | --------------------------- |
| `gitlab_backup_s3_access_key` | `env_GITLAB_BACKUP_S3_ACCESS_KEY` | Access key id |
| `gitlab_backup_s3_secret_key` | `env_GITLAB_BACKUP_S3_SECRET_KEY` | Secret key |
| `gitlab_backup_s3_url` | `env_GITLAB_BACKUP_S3_URL` | URL path for storage (default - `https://s3.amazonaws.com`) |
| `gitlab_backup_s3_encrypt` | `env_GITLAB_BACKUP_S3_ENCRYPT` | Enable requests server-side encryption (for AWS forced yes, default else - no) |

### Additional variables

Read the Readme file to the role of vst_gitlab.

### License

GNU GPLv3

### `Enjoy it!`
